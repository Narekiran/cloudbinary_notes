How To Install MySQL 8 on Amazon Linux 2

#### 
```
Configure Yum Repository:
    
Most of the packages required the dependencies that are available in other third-party repositories. Use the following command to configure the EPEL repository that is required for package installation.

    $ sudo amazon-linux-extras install epel -y

Then configure the MySQL repository by installing the package provided by the MySQL official site.

    $ sudo yum install https://dev.mysql.com/get/mysql80-community-release-el7-5.noarch.rpm 

Install MySQL Server:

You can successfully configure the repositories, your system is ready for MySQL installation. Execute the below-mentioned command to install MySQL 8 community server on Amazon Linux.

    $ sudo yum install mysql-community-server 


Activate and Start MySQL Service:

Once the installation is successfully finished. The default MySQL service will be stopped and in an inactive state. First, use the following commands to activate the service to auto-start on system startup, then start it manually for the first time.

    $ systemctl enable mysqld 
    
    $ systemctl start mysqld 

Then, use the following command to view the MySQL service status. It should be active and running.

    $ systemctl status mysqld 

Find initial root password:

During the installation of packages, an initial password is configured for the MySQL root account. You can find this password from the MySQL log file.

    $ cat /var/log/mysqld.log | grep "A temporary password" 

You will see the output below that includes the default root password.

MySQL Post Installation Setup:

A post-installation script is provided by the MySQL packages. That is helpful for configuring MySQL once after the installation. This helps us to configure a strong root account password, remote anonymous users, disallow root access remotely and remove the test database.

Execute the following command from the terminal:

    $ sudo mysql_secure_installation 

Enter the root password found in the above step, then set a new password for the MySQL root account. Next, follow the onscreen instructions and Press Y for all other operations to apply improved security.

Enter password for user root: [Enter current root password]
New password: [Enter a new root password]
Re-enter new password: [Re-Enter the new root password]
Estimated strength of the password: 100
Change the password for root ? ((Press y|Y for Yes, any other key for No) : n
Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
Disallow root login remotely? (Press y|Y for Yes, any other key for No) : y
Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y
Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y
All done!

Connect to MySQL:

Your MySQL server is ready to use now. From the terminal, you can run the below command to connect to the MySQL command line interface. It will prompt for the root account password. On successful authentication, you will get the MySQL prompt.

    $ mysql -u root -p 


```
